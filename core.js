var leftOperand, operator;
var input = document.getElementById("result");
var buttons = document.querySelectorAll('input[type=button]');


//for 0...9
function addNumber() {
    if (input.value.substr(0, 1) == '0' && input.value.substr(1,1) != '.')
        input.value = '';

    input.value += this.value;
}

//for .
function dot() {
    if (input.value.length == 0)
        input.value = '0';

    if (input.value.indexOf(".") == -1)
        input.value += this.value; 
}


//for +, -, *, /
function action() {

    calculate();

    if (input.value.length) {
        leftOperand = parseFloat(input.value);
        operator = this.value;
        input.value = '0';
    }
}

// for =
function calculate () {
    if (leftOperand != undefined && operator != undefined && input.value.length) {
        var rightOperand = parseFloat(input.value);
        var result;
        switch (operator) {
            case '+':
                result = leftOperand + rightOperand;
                break;
            case '−':
                result = leftOperand - rightOperand;
                break;
            case '×':
                result = leftOperand * rightOperand;
                break;
            case '÷':
                result = leftOperand / rightOperand;
                break;
            default:
                return;
        }

        leftOperand = undefined;
        operator = undefined;
        input.value = result;
    }
}

//for C
function clearNumber() {
    input.value = '0';
    leftOperand = undefined;
    operator = undefined;
}

//for <-
function remove() {
    input.value = input.value.substr(0, input.value.length - 1);
    if (input.value.length == 0) {
        input.value = '0';
    }
}

//for %
function percent() {
    input.value = parseFloat(input.value) / 100;
}

// for +/-
function negative() {
    if (input.value.length == 0)
        return;

    if (input.value.substr(0, 1) == '-') {
        input.value = input.value.substr(1);
    }
    else {
        input.value = '-' + input.value;
    }
}

for(var key in buttons) {
    switch(buttons[key].value) {
        case '0':
        case '1':
        case '2':
        case '3':
        case '4':
        case '5':
        case '6':
        case '7':
        case '8':
        case '9':
            buttons[key].addEventListener("click", addNumber);
            break;
        case '.':
            buttons[key].addEventListener("click", dot);
            break;
        case '÷':
        case '×':
        case '−':
        case '+':
            buttons[key].addEventListener("click", action);
            break;
        case '=':
            buttons[key].addEventListener("click", calculate);
            break;
        case 'C':
            buttons[key].addEventListener("click", clearNumber);
            break;
        case '←':
            buttons[key].addEventListener("click", remove);
            break;
        case '%':
            buttons[key].addEventListener("click", percent);
            break;
        case '±':
            buttons[key].addEventListener("click", negative);
            break;
    }
}

